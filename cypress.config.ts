import { defineConfig } from 'cypress';

export default defineConfig({
  component: {
    devServer: {
      framework: 'react',
      bundler: 'webpack',
    },
    specPattern: 'tests/components/**/*.cy.{js,jsx,ts,tsx}',
    supportFile: false,
  },
  e2e: {
    baseUrl: 'http://localhost:8080',
    specPattern: 'tests/integration/**/*.cy.{js,jsx,ts,tsx}',
    supportFile: false,
  },
});
