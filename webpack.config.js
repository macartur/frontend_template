const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
module.exports = {
  mode: "development",
  entry: "./src/main/index.tsx",
  output: {
    path: path.join(__dirname, 'public/js'),
    filename: "bundle.js",
    publicPath: "/public/js"
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    alias: {
      '@': path.join(__dirname, 'src')
    }
  },
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        loader: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        loader: 'css-loader',
        exclude: /node_modules/
      }
    ]
  },
  devServer: {
    static: "./public",
    devMiddleware: {
      writeToDisk: true,
    },
    historyApiFallback: true // map react routes
  },
  externals: {
    react: "React",
    'react-dom': "ReactDOM"
  },
  plugins: [
    new CleanWebpackPlugin()
  ]
}
